<?php

/**
 * @file
 * uw_user_tutoring_rule.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uw_user_tutoring_rule_default_rules_configuration() {
  $items = array();
  $items['rules_redirect_to_tutoring_page_after_accept_terms'] = entity_import('rules_config', '{ "rules_redirect_to_tutoring_page_after_accept_terms" : {
      "LABEL" : "Redirect to tutoring page after accept terms",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "tutoring" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "uw_tutoring_legal" : "uw_tutoring_legal" } }
          }
        },
        { "NOT data_is_empty" : { "data" : [ "node:field-tutoring-legal-accept" ] } },
        { "node_is_published" : { "node" : [ "node" ] } }
      ],
      "DO" : [ { "redirect" : { "url" : "tutoring" } } ]
    }
  }');
  return $items;
}
