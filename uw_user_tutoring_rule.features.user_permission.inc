<?php
/**
 * @file
 * uw_user_tutoring_rule.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_user_tutoring_rule_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'Administer tutoring message configuration'.
  $permissions['Administer tutoring message configuration'] = array(
    'name' => 'Administer tutoring message configuration',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_user_tutoring_rule',
  );

  return $permissions;
}
